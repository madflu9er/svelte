// export const ChartTypes = {
//     area: "area",
//     areaSpline: "area-spline",
//     bar: "bar",
//     spline: "spline",
//     step: "step",
//     areaStep: "area-step",
//     scatter: "scatter",
//     pie: "pie",
//     donut: "donut",
//     gauge: "gauge",
// };


export const ChartTypes = [
    {name: 'Area', type: "area"},
    {name: 'Area-Spline', type: "area-spline"},
    {name: 'Bar', type: "bar"},
    {name: 'Spline', type: "spline"},
    {name: 'Area-Step', type: "area-step"},
    {name: 'Step', type: "step"},
    {name: 'Default', type: ""},
]
